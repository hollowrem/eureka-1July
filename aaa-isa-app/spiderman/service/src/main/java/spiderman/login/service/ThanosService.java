package spiderman.login.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spiderman.login.dao.DepositRequest;
import spiderman.login.dao.LoginRequest;
import spiderman.login.dao.ThanosInterface;

@Service
public class ThanosService {
	@Autowired
	private ThanosInterface thaInter;

	public Object Login(String nama, String password) {
		LoginRequest logReq = new LoginRequest();
		logReq.setNama(nama);
		logReq.setPassword(password);
		return thaInter.Login(logReq);
	}

	public Object CekSaldo(String nomor) {
		DepositRequest depoReq = new DepositRequest();
		depoReq.setNomor(nomor);
		return thaInter.CekSaldo(depoReq);
	}

	public Object Setor(String nomor, String amount) {
		DepositRequest depoReq = new DepositRequest();
		depoReq.setNomor(nomor);
		depoReq.setAmount(amount);
		return thaInter.Setor(depoReq);
	}

	public Object Tarik(String nomor, String amount) {
		DepositRequest depoReq = new DepositRequest();
		depoReq.setNomor(nomor);
		depoReq.setAmount(amount);
		return thaInter.Tarik(depoReq);
	}

	public List LsHisto() {
		return thaInter.LsHisto();
	}

}
