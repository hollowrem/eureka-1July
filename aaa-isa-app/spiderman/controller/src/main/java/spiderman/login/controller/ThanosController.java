package spiderman.login.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spiderman.login.dao.DepositRequest;
import spiderman.login.dao.LoginRequest;
import spiderman.login.response.BaseResponse;
import spiderman.login.service.ThanosService;

@RefreshScope
@RestController
@RequestMapping("/thanos")
public class ThanosController {
	@Autowired
	ThanosService thaSrc;

	// private SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yy (hh:mm:ss)");

	/*
	 * @RequestMapping(value="/login",method=RequestMethod.POST) public BaseResponse
	 * Login(@RequestBody LoginRequest obj){ BaseResponse respon = new
	 * BaseResponse(); String user = logSrc.login(obj.getNama(),obj.getPassword());
	 * 
	 * respon.setResponseCode("Time : "+sdf.format(new Date()));
	 * //if(user.equalsIgnoreCase("00")){ // respon.setResponseDesc(user); //}
	 * respon.setResponseDesc(user); return respon; }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Object Login(@RequestBody LoginRequest logReq) {
		Object run1 = thaSrc.Login(logReq.getNama(), logReq.getPassword());
		return run1;
	}

	@RequestMapping(value = "/ceksaldo", method = RequestMethod.POST)
	public Object CekSaldo(@RequestBody DepositRequest depoReq) {
		Object run1 = thaSrc.CekSaldo(depoReq.getNomor());
		return run1;
	}

	@RequestMapping(value = "/setor", method = RequestMethod.POST)
	public Object Setor(@RequestBody DepositRequest depoReq) {
		Object run1 = thaSrc.Setor(depoReq.getNomor(), depoReq.getAmount());
		return run1;
	}

	@RequestMapping(value = "/tarik", method = RequestMethod.POST)
	public Object Tarik(@RequestBody DepositRequest depoReq) {
		Object run1 = thaSrc.Tarik(depoReq.getNomor(), depoReq.getAmount());
		return run1;
	}

	@RequestMapping(value = "/history", method = RequestMethod.POST)
	public List LsHisto() {
		List run1 = thaSrc.LsHisto();
		return run1;
	}

}
