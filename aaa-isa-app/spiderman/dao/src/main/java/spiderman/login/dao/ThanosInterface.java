package spiderman.login.dao;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;


@FeignClient("thanos")
public interface ThanosInterface {
	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value = "3000")
	@RequestMapping(value = "/thanos/login", method = RequestMethod.POST)
	public Object Login(@RequestBody LoginRequest logReq);

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value = "3000")
	@RequestMapping(value = "/thanos/ceksaldo", method = RequestMethod.POST)
	public Object CekSaldo(@RequestBody DepositRequest depoReq);

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value = "3000")
	@RequestMapping(value = "/thanos/setor", method = RequestMethod.POST)
	public Object Setor(@RequestBody DepositRequest depoReq);

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value = "3000")
	@RequestMapping(value = "/thanos/tarik", method = RequestMethod.POST)
	public Object Tarik(@RequestBody DepositRequest depoReq);

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value = "3000")
	@RequestMapping(value = "/thanos/history", method = RequestMethod.POST)
	public List LsHisto();

}
