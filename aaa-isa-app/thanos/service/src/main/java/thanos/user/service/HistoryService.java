package thanos.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import thanos.user.dao.model.History;
import thanos.user.dao.repository.HistoryRepository;
import thanos.user.util.CollectionHelper;

@Service
public class HistoryService {
	@Autowired
	private HistoryRepository repository;

	@Cacheable(value = "thanos.history.cek", unless = "#result == null")
	@SuppressWarnings("unchecked")
	public List<History> findAll() {
		return CollectionHelper.iterToList(repository.findAll());
	}

	@CacheEvict(value = "thanos.history.cek", beforeInvocation = true, allEntries=true)
	public History save(History history) {
		return repository.save(history);
	}

}
