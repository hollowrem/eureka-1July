package thanos.user.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import thanos.user.dao.model.History;
import thanos.user.dao.model.Rekening;
import thanos.user.dao.repository.RekeningRepository;
import thanos.user.util.CollectionHelper;

@Service
public class RekeningService {

	@Autowired
	private RekeningRepository repository;

	@Autowired
	private HistoryService histoSrc;

	public Rekening findById(Integer id) {
		return repository.findById(id).get();
	}

	@Cacheable(value = "thanos.rekening.ceksaldo", unless = "#result == null")
	public Rekening findByNomor(String nomor) {
		return repository.findByNomor(nomor);
	}

	public Rekening findByNomorAndSaldo(String nomor, String saldo) {
		return repository.findByNomorAndSaldo(nomor, saldo);
	}

	@SuppressWarnings("unchecked")
	public List<Rekening> findAll() {
		return CollectionHelper.iterToList(repository.findAll());
	}

	@CacheEvict(value = "thanos.rekening.ceksaldo", allEntries = true, beforeInvocation = true)
	public Rekening save(Rekening rekening) {
		return repository.save(rekening);
	}
	
	@CacheEvict(value = "thanos.rekening.ceksaldo", allEntries = true, beforeInvocation = true)
	public void ClearCekSaldo() {}
	
	@Transactional
	public ArrayList<String> DepositRek(String nomor, String amount, String saldoAwal, String saldoAkhir) {
		ArrayList<String> als = new ArrayList<String>();
		ClearCekSaldo();
		Rekening rekening = repository.findByNomor(nomor);
		Integer saldo = Integer.valueOf(rekening.getSaldo());
		Integer jml = Integer.valueOf(amount);
		Integer saldoFinal = saldo + jml;

		History histo = new History();
		histo.setDescription(
				"Saldo rek: " + nomor + " bertambah: " + jml.toString() + " balance: " + saldoFinal.toString());
		histoSrc.save(histo);

		rekening.setSaldo(saldoFinal.toString());
//		ClearCekSaldo();
		als.add(nomor);
		als.add(amount);
		als.add(saldo.toString());
		als.add(saldoFinal.toString());
		return als;
	}

	@Transactional
	public ArrayList<String> Withdraw(String nomor, String amount, String saldoAwal, String saldoAkhir) {
		ArrayList<String> als = new ArrayList<String>();
		ClearCekSaldo();
		Rekening rekening = repository.findByNomor(nomor);
		Integer saldo = Integer.valueOf(rekening.getSaldo());
		Integer jml = Integer.valueOf(amount);
		Integer saldoFinal = saldo - jml;

		if (saldoFinal < 0) {
//			ClearCekSaldo();
			return als;
		} else {
			History histo = new History();
			histo.setDescription(
					"Saldo rek: " + nomor + " berkurang: " + jml.toString() + " balance: " + saldoFinal.toString());
			histoSrc.save(histo);

			rekening.setSaldo(saldoFinal.toString());
//			ClearCekSaldo();
			als.add(nomor);
			als.add(amount);
			als.add(saldo.toString());
			als.add(saldoFinal.toString());
			return als;
		}

	}
}
