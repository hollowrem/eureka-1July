package thanos.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import thanos.user.dao.model.Usera;
import thanos.user.dao.repository.UseraRepository;
import thanos.user.util.CollectionHelper;

@Service("UseraService")
public class UseraService {

	@Autowired
	private UseraRepository usrRepo;

	public Usera findById(Long id) {
		return usrRepo.findById(id).get();
	}

	// hazelcast ready
	// , unless = "#result == null")
	@Cacheable(value = "thanos.user.findByNamaAndPassword", unless = "#result == null")
	public Usera findByNamaAndPassword(String nama, String password) {
		return usrRepo.findByNamaAndPassword(nama, password);
	}

	// @Caching(evict = {
	// @CacheEvict ( value = "thanos.user.findByNamaAndPassword", beforeInvocation =
	// true)
	// } ) //multipe cache evict ??Causing Eclipse Content Assist Crash??
	// @CacheEvict(value = "thanos.user.findByNamaAndPassword", beforeInvocation =
	// true) //1 cache evict

	@SuppressWarnings("unchecked")
	public List<Usera> findAll() {
		return CollectionHelper.iterToList(usrRepo.findAll());
	}

	// @CacheEvict(value="thanos.user.findByNamaAndPassword", allEntries = true)
	// //allentries=true not working??
	@CacheEvict(value = "thanos.user.findByNamaAndPassword", beforeInvocation = true, allEntries = true)
	public Usera save(Usera Usera) {
		return usrRepo.save(Usera);
	}

}
