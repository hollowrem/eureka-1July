package thanos.user.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import thanos.user.dao.model.History;
import thanos.user.dao.model.Rekening;
import thanos.user.request.DepositRequest;
import thanos.user.response.AllResponse;
import thanos.user.response.BaseResponse;
import thanos.user.response.CekSaldoResponse;
import thanos.user.response.TransactionResponse;
import thanos.user.service.HistoryService;
import thanos.user.service.RekeningService;

@EnableCaching
@RestController
// @RequestMapping("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionController {

	@Autowired
	private RekeningService rekeningService;
	@Autowired
	private HistoryService histoSrc;

	// TUGAS eureka zuul no 2 cek saldo plus history
	@POST
	@RequestMapping("/ceksaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	public CekSaldoResponse CekSaldo(@RequestBody DepositRequest depoReq) {
		CekSaldoResponse cek1 = new CekSaldoResponse();
		try {
			Rekening rek1 = rekeningService.findByNomor(depoReq.getNomor());

			History histo = new History();
			histo.setDescription("Cek saldo rek: " + rek1.getNomor());
			histoSrc.save(histo);

			cek1.setNomor(rek1.getNomor());
			cek1.setSaldo(rek1.getSaldo());
			cek1.setError("Berhasil cek saldo");
			return cek1;
		} catch (Exception e) {
			cek1.setError("Gagal cek saldo");
			return cek1;
		}

	}

	// TUGAS eureka zuul no 3 setor plus history
	@POST
	@RequestMapping("/setor")
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionResponse DepositRek(@RequestBody DepositRequest depoReq) {
		TransactionResponse trs = new TransactionResponse();
		try {
			ArrayList<String> rekupd = rekeningService.DepositRek(depoReq.getNomor(), depoReq.getAmount(), null, null);

			trs.setNomor(rekupd.get(0));
			trs.setSaldoAwal(rekupd.get(2));
			trs.setSaldoAkhir(rekupd.get(3));
			trs.setError("Berhasil tambah saldo");
			rekeningService.ClearCekSaldo();//test counter cacheable
			return trs;
		} catch (Exception e) {
			trs.setError("Gagal tambah saldo, ");
			return trs;
		}
	}

	// TUGAS eureka zuul no 4 tarik plus history
	@POST
	@RequestMapping("/tarik")
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionResponse Withdraw(@RequestBody DepositRequest depoReq) {
		TransactionResponse trs = new TransactionResponse();
		try {
			ArrayList<String> rekupd = rekeningService.Withdraw(depoReq.getNomor(), depoReq.getAmount(), null, null);
			if (rekupd.isEmpty()) {
				trs.setError("Saldo tidak mencukupi");
				return trs;
			}

			trs.setNomor(rekupd.get(0));
			trs.setSaldoAwal(rekupd.get(2));
			trs.setSaldoAkhir(rekupd.get(3));
			trs.setError("Berhasil tarik uang");
			rekeningService.ClearCekSaldo();//test counter cacheable
			return trs;
		} catch (Exception e) {
			trs.setError("Gagal tarik uang");
			return trs;
		}

	}

	@RequestMapping("/history")
	public List<History> lsHisto() {
		return histoSrc.findAll();
	}

	@POST
	@RequestMapping("/addrek")
	@Consumes(MediaType.APPLICATION_JSON)
	public String AddRekening(@RequestBody Rekening rekReq) {
		Rekening rekAdd = new Rekening();
		rekAdd.setNomor(rekReq.getNomor());
		rekAdd.setSaldo(rekReq.getSaldo());

		try {
			rekeningService.save(rekAdd);
			return "Berhasil tambah rekening";
		} catch (Exception e) {
			return "Gagal tambah rekening " + e.getMessage();
		}

	}

}
