package thanos.user.dao.repository;

import org.springframework.data.repository.CrudRepository;

import thanos.user.dao.model.Rekening;

public interface RekeningRepository extends CrudRepository<Rekening, Integer> {

	public abstract Rekening findByNomor(String nomor);

	public abstract Rekening findByNomorAndSaldo(String nomor, String saldo);

}
