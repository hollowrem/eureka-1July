package thanos.user.dao.repository;

import org.springframework.data.repository.CrudRepository;

import thanos.user.dao.model.History;

public interface HistoryRepository extends CrudRepository<History, Integer>{

}
